﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Repo
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class DVDRentalDatabaseEntities : DbContext
    {
        public DVDRentalDatabaseEntities()
            : base("name=DVDRentalDatabaseEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<Actor> Actor { get; set; }
        public virtual DbSet<Branch> Branch { get; set; }
        public virtual DbSet<BranchMember> BranchMember { get; set; }
        public virtual DbSet<Category> Category { get; set; }
        public virtual DbSet<Director> Director { get; set; }
        public virtual DbSet<DVD> DVD { get; set; }
        public virtual DbSet<Member> Member { get; set; }
        public virtual DbSet<Movie> Movie { get; set; }
        public virtual DbSet<MovieActor> MovieActor { get; set; }
        public virtual DbSet<MovieCategory> MovieCategory { get; set; }
        public virtual DbSet<Rent> Rent { get; set; }
        public virtual DbSet<Staff> Staff { get; set; }
        public virtual DbSet<database_firewall_rules> database_firewall_rules { get; set; }
    }
}
