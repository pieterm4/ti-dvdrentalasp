﻿using System.Threading.Tasks;
using System.Web.Mvc;
using DVDRentalASP.Helpers;
using DVDRentalASP.Repository;

namespace DVDRentalASP.Controllers
{
    public class RegistrationController : Controller
    {
        private readonly dvdrent_dbEntities db = new dvdrent_dbEntities();
        private readonly Repository<Member> _memberRepository;

        public RegistrationController()
        {
            _memberRepository = new Repository<Member>(db);
        }


        // GET: Registration
        public ActionResult RegisterMember()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Register(Member member)
        {
            if (member != null)
            {
                RandomIndex<Member> index = new RandomIndex<Member>(_memberRepository);
                var freeindex = await index.GetFreeIndex();
                member.memberNumber = freeindex;
                await _memberRepository.Insert(member);

                return Redirect("MemberPanel");
            }

            return Redirect("Index");
        }


    }
}