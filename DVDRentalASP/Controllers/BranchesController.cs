﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web.Mvc;
using DVDRentalASP.Helpers;
using DVDRentalASP.Repository;

namespace DVDRentalASP.Controllers
{
    public class BranchesController : Controller
    {
        private dvdrent_dbEntities db = new dvdrent_dbEntities();
        private readonly Repository<Branch> _branchRepository;

        public BranchesController()
        {
            _branchRepository = new Repository<Branch>(db);
        }

        // GET: Branches
        public async Task<ActionResult> Index()
        {
            var branch = db.Branch.Include(b => b.Staff1);
            if (branch == null) throw new ArgumentNullException(nameof(branch));
            return View(await branch.ToListAsync());
        }

        // GET: Branches/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var branch = await _branchRepository.Get(id);
            if (branch == null)
            {
                return HttpNotFound();
            }
            return View(branch);
        }

        // GET: Branches/Create
        public ActionResult Create()
        {
            ViewBag.managerID = new SelectList(db.Staff, "staffNumber", "name");
            return View();
        }

        

        // POST: Branches/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "managerID,street,city,state,zippCode,phoneNumber")] Branch branch)
        {
            var random = new RandomIndex<Branch>(_branchRepository);
            branch.branchNumber = await random.GetFreeIndex();
            
            if (ModelState.IsValid)
            {
                await _branchRepository.Insert(branch);
                await _branchRepository.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.managerID = new SelectList(db.Staff, "staffNumber", "name", branch.managerID);
            return View(branch);
        }

        // GET: Branches/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var branch = await _branchRepository.Get(id);
            if (branch == null)
            {
                return HttpNotFound();
            }
            ViewBag.managerID = new SelectList(db.Staff, "staffNumber", "name", branch.managerID);
            return View(branch);
        }

        // POST: Branches/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "branchNumber,managerID,street,city,state,zippCode,phoneNumber")] Branch branch)
        {
            if (ModelState.IsValid)
            {
                db.Entry(branch).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.managerID = new SelectList(db.Staff, "staffNumber", "name", branch.managerID);
            return View(branch);
        }

        // GET: Branches/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Branch branch = await db.Branch.FindAsync(id);
            if (branch == null)
            {
                return HttpNotFound();
            }
            return View(branch);
        }

        // POST: Branches/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Branch branch = await db.Branch.FindAsync(id);
            db.Branch.Remove(branch);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
