﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DVDRentalASP;
using DVDRentalASP.Helpers;
using DVDRentalASP.Repository;
using JetBrains.Reflection;

namespace DVDRentalASP.Controllers
{
    public class MoviesController : Controller
    {
        private readonly dvdrent_dbEntities db = new dvdrent_dbEntities();

        private readonly Repository<Movie> _movieRepository;
        private readonly Repository<Director> _directoRepository;
        private readonly Repository<Category> _categoryRepository;

        private static int _editCategoryIndex;

        public MoviesController()
        {
            _movieRepository = new Repository<Movie>(db);
            _directoRepository = new Repository<Director>(db);
            _categoryRepository=new Repository<Category>(db);
        }

        // GET: Movies
        public async Task<ActionResult> Index()
        {
            var movie = db.Movie.Include(m => m.Branch).Include(m => m.Director);
            return View(await movie.ToListAsync());
        }

        // GET: Movies/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Movie movie = await db.Movie.FindAsync(id);
            if (movie == null)
            {
                return HttpNotFound();
            }
            return View(movie);
        }

        // GET: Movies/Create
        public ActionResult Create()
        {
            ViewBag.branchNumber = new SelectList(db.Branch, "branchNumber", "street");
            ViewBag.directorNumber = new SelectList(db.Director, "directorNumber", "name");
            ViewBag.categories = new SelectList(db.Category, "categoryNumber", "name");
            return View();
        }

        // POST: Movies/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(int branchNumber, int directorNumber, int[] categories, string title, decimal dailyRent)
        {
            List<Category> cat = new List<Category>();
            foreach (var category in categories)
            {
                cat.Add(await _categoryRepository.Get(category));
            }
            var movie = new Movie()
            {
                branchNumber = branchNumber,
                directorNumber = directorNumber,
                title = title,
                dailyRent = dailyRent,
                Category = new List<Category>(cat)
                    
                    
            };

            if (ModelState.IsValid)
            {
                
                var ran = new RandomIndex<Movie>(_movieRepository);
                var index = await ran.GetFreeIndex();
                movie.catalogNumber = index;
                db.Movie.Add(movie);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            //ViewBag.branchNumber = new SelectList(db.Branch, "branchNumber", "street", movie.branchNumber);
            //ViewBag.directorNumber = new SelectList(db.Director, "directorNumber", "name", movie.directorNumber);
            //ViewBag.categories = new SelectList(db.Category, "categoryNumber", "name", movie.Category);
            return Redirect("Index");
        }

        // GET: Movies/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Movie movie = await db.Movie.FindAsync(id);
            if (movie == null)
            {
                return HttpNotFound();
            }

            _editCategoryIndex = movie.catalogNumber;
            ViewBag.branchNumber = new SelectList(db.Branch, "branchNumber", "street", movie.branchNumber);
            ViewBag.directorNumber = new SelectList(db.Director, "directorNumber", "name", movie.directorNumber);
            ViewBag.categories = new SelectList(db.Category, "categoryNumber", "name", movie.Category);
            return View(movie);
        }

        // POST: Movies/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(int branchNumber, int directorNumber, int[] categories, string title, decimal dailyRent)
        {

            List<Category> cat = new List<Category>();
            foreach (var category in categories)
            {
                cat.Add(await _categoryRepository.Get(category));
            }
            var movie = new Movie()
            {
                catalogNumber = _editCategoryIndex,
                branchNumber = branchNumber,
                directorNumber = directorNumber,
                title = title,
                dailyRent = dailyRent,
                Category = new List<Category>(cat)
                    
            };


            if (ModelState.IsValid)
            {
                db.Entry(movie).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.branchNumber = new SelectList(db.Branch, "branchNumber", "street", movie.branchNumber);
            ViewBag.directorNumber = new SelectList(db.Director, "directorNumber", "name", movie.directorNumber);
            ViewBag.categories = new SelectList(db.Category, "categoryNumber", "name", movie.Category.FirstOrDefault());
            return View(movie);
        }

        // GET: Movies/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Movie movie = await db.Movie.FindAsync(id);
            if (movie == null)
            {
                return HttpNotFound();
            }
            return View(movie);
        }

        // POST: Movies/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Movie movie = db.Movie.Include(a => a.Category).SingleOrDefault(a => a.catalogNumber == id);
            var director =
                db.Director.Include(m => m.Movie).SingleOrDefault(a => a.directorNumber == movie.directorNumber);
            director?.Movie.Remove(movie);


            
            await _directoRepository.Update(director);
            if (movie != null)
            {
                foreach (var cat in movie.Category.ToList())
                {
                    movie.Category.Remove(cat);
                }

                foreach (var act in movie.Actor.ToList())
                {
                    movie.Actor.Remove(act);
                }

                foreach (var dvds in movie.DVD.ToList())
                {
                    movie.DVD.Remove(dvds);
                }

                //await db.SaveChangesAsync();


            }

            db.Movie.Remove(movie);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
