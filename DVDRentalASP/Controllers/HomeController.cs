﻿using System.Web.Mvc;

namespace DVDRentalASP.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Stay in touch with us...";

            return View();
        }

        public ActionResult AdminPanel()
        {
            return View();
        }
    }
}