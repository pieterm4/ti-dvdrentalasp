﻿using System;
using System.Threading.Tasks;
using DVDRentalASP.Repository;

namespace DVDRentalASP.Helpers
{
    public class RandomIndex<T> where T: class, new()
    {
        private readonly IRepository<T> _context;

        public RandomIndex(IRepository<T> context)
        {
            _context = context;
        }

        private int RandomNumber()
        {
            var rand = new Random();
            var next = rand.Next(0, int.MaxValue);

            return next;
        }

        public async Task<int> GetFreeIndex()
        {
            while (true)
            {
                var randomized = RandomNumber();
                var elementList = await _context.Get(randomized);

                if (elementList == null)
                    return randomized;
            }
        }
    }
}