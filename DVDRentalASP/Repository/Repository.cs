﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Linq;

namespace DVDRentalASP.Repository
{
    public class Repository<T> : IRepository<T> where T : class, new()
    {

        private readonly DbSet<T> _entitySet;
        private readonly DbContext _context;

        public Repository(DbContext context)
        {
            if (context == null)
                throw new ArgumentNullException(nameof(context));

            _context = context;
            _entitySet = context.Set<T>();
        }

        public async Task Delete(T entity)
        {
            _entitySet.Remove(entity);
        }

        public async Task<List<T>> Get()
        {
            return await _entitySet.ToListAsync();
        }

        public async Task<T> Get(int? id)
        {
            return await _entitySet.FindAsync(id);
        }

        public IEnumerable<T> Get(Expression<Func<T, bool>> predicate)
        {
            return _entitySet.Where(predicate);
        }

        public async Task Insert(T entity)
        {
            if(entity != null)
                _entitySet.Add(entity);
        }

        public async Task Update(T entity)
        {
            if (entity != null)
                _entitySet.Attach(entity);
        }

        public async Task SaveChanges()
        {
            await _context.SaveChangesAsync();
        }
    }
}